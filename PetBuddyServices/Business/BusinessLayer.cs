﻿using AutoMapper;
using PetBuddyServices.Models;
using PetBuddyServices.Models.TransportModels;
using PetBuddyServices.Persistance;
using System.Collections.Generic;

namespace PetBuddyServices.Business
{
    public class BusinessLayer
    {
        private readonly DbPersist _persist;

        public BusinessLayer()
        {
            _persist = new DbPersist();
        }

        public TokenData Login(Login loginData)
        {
            var login = _persist.Login(loginData.Email, loginData.Password);
            return new TokenData {Token = login.token, ValidUntil = login.enddate};
        }

        public SitterModel LoadSitter(string token)
        {
            return Mapper.Map<Sitter, SitterModel>(_persist.LoadSitter(token));
        }

        public SitterModel CreateSitter(SitterModel sitter)
        {
            Sitter newSitter = _persist.CreateSitter(Mapper.Map<SitterModel, Sitter>(sitter), Mapper.Map<SitterModel, sitter_auth>(sitter));

            return Mapper.Map<Sitter,SitterModel>(newSitter);
        }

        public SitterModel UpdateSitter(string token, SitterModel sitter)
        {
            Sitter updatedSitter = _persist.UpdateSitter(token, Mapper.Map<SitterModel, Sitter>(sitter));

            return Mapper.Map<Sitter, SitterModel>(updatedSitter);
        }

        public void ChangePassword(string token, string oldPw, string newPw)
        {
            _persist.ChangePassword(token, oldPw, newPw);
        }
        
        public SitterModel AddPet(string token, PetModel newPet)
        {
            Sitter sitter = _persist.AddPet(token, Mapper.Map<PetModel, Pet>(newPet));
            return Mapper.Map<Sitter, SitterModel>(sitter);
        }

        public SitterModel UpdatePet(string token, int petid, PetModel newPet)
        {
            Sitter sitter = _persist.UpdatePet(token, petid, Mapper.Map<PetModel, Pet>(newPet));
            return Mapper.Map<Sitter, SitterModel>(sitter);
        }

        public void DeletePet(string token, int petid)
        {
            _persist.DeletePet(token, petid);
        }

        public List<PetServiceModel> LoadAllServices()
        {
            return _persist.LoadAllServices().ConvertAll(Mapper.Map<PetService, PetServiceModel>);
        }

        public PetServiceModel LoadService(int serviceId)
        {
            return Mapper.Map<PetService, PetServiceModel>(_persist.LoadService(serviceId));
        }

        public void DeleteService(int serviceId)
        {
            _persist.DeleteService(serviceId);
        }

        public PetServiceModel CreateService(string token, PetServiceModel newService)
        {
            var retValue = _persist.CreateService(token, Mapper.Map<PetServiceModel, PetService>(newService));
            return Mapper.Map<PetService, PetServiceModel>(retValue);
        }

        public PetServiceModel UpdateService(int id, PetServiceModel newService)
        {
            var retValue = _persist.UpdateService(id, Mapper.Map<PetServiceModel, PetService>(newService));
            return Mapper.Map<PetService, PetServiceModel>(retValue);
        }
    }
}