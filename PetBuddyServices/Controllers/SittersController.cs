﻿using System.Web.Http.Cors;
using PetBuddyServices.Business;
using PetBuddyServices.Models;
using PetBuddyServices.Models.TransportModels;
using System.Web.Http;

namespace PetBuddyServices.Controllers
{
   
    public class SittersController : ApiController
    {
        private readonly BusinessLayer _layer;

        public SittersController()
        {
            _layer = new BusinessLayer();
        }

        /// <summary>
        /// gives you the sitter and it's data by id
        /// </summary>
        /// <param name="token">the login toke of the sitter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("sitters/{token}")]
        public SitterModel Get(string token)
        {
            return _layer.LoadSitter(token);
        }

        /// <summary>
        /// gives you the sitter and it's data by id
        /// </summary>
        /// <param name="loginData">The needed data to verify the sitters login</param>
        /// <returns></returns>
        [HttpPost]
        [Route("sitters/login")]
        [AcceptVerbs("OPTIONS")]
        public TokenData Login(Login loginData)
        {
            return _layer.Login(loginData);
        }

        /// <summary>
        /// let's you create a new sitter account
        /// </summary>
        /// <param name="sitter">the sitter that should be created</param>
        [HttpPost]
        [Route("sitters")]
        [AcceptVerbs("OPTIONS")]
        public SitterModel CreateSitter([FromBody]SitterModel sitter)
        {
            return _layer.CreateSitter(sitter);
        }

        /// <summary>
        /// let's you update a sitter
        /// </summary>
        /// <param name="token">the login token of the sitter</param>
        /// <param name="sitter">the new data of the sitter</param>
        [HttpPut]
        [Route("sitters/{token}")]
        [AcceptVerbs("OPTIONS")]
        public SitterModel Put(string token, [FromBody]SitterModel sitter)
        {
            return _layer.UpdateSitter(token, sitter);
        }

        /// <summary>
        /// let's you update a sitters password
        /// </summary>
        /// <param name="token">the login token of the sitter</param>
        /// <param name="password">the old and new password of the sitter account</param>
        [HttpPut]
        [Route("sitters/{token}/password")]
        [AcceptVerbs("OPTIONS")]
        public void ChangePassword(string token, [FromBody]ChangePw password)
        {
            _layer.ChangePassword(token, password.OldPassword, password.NewPassword);
        }

        /// <summary>
        /// let's you update a sitter
        /// </summary>
        /// <param name="token">the login token of the sitter</param>
        /// <param name="pet">the new pet that should be added</param>
        [HttpPost]
        [Route("sitters/{token}/pet")]
        [AcceptVerbs("OPTIONS")]
        public SitterModel AddPet(string token, [FromBody]PetModel pet)
        {
            return _layer.AddPet(token, pet);
        }

        /// <summary>
        /// let's you update a sitter
        /// </summary>
        /// <param name="token">the login token of the sitter</param>
        /// <param name="petid">the id pf the pet that should be updated</param>
        /// <param name="pet">the actual pet data that should be safed</param>
        [HttpPut]
        [Route("sitters/{token}/pet/{petid}")]
        [AcceptVerbs("OPTIONS")]
        public SitterModel UpdatePet(string token, int petid, [FromBody]PetModel pet)
        {
            return _layer.UpdatePet(token, petid, pet);
        }

        /// <summary>
        /// let's you delete a pet by it's id
        /// </summary>
        /// <param name="token">the login token of the sitter</param>
        /// <param name="petId">the id of the pet that should be deleted</param>
        [HttpDelete]
        [Route("sitters/{token}/pet/{petId}")]
        [AcceptVerbs("OPTIONS")]
        public void DeletePet(string token, int petId)
        {
            _layer.DeletePet(token, petId);
        }
    }
}