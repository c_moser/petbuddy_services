﻿using PetBuddyServices.Business;
using PetBuddyServices.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PetBuddyServices.Controllers
{

    public class PetServiceController : ApiController
    {
        private readonly BusinessLayer _layer;

        public PetServiceController()
        {
            _layer = new BusinessLayer();
        }

        /// <summary>
        /// Loads all available services
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("service")]
        public List<PetServiceModel> Get()
        {
            return _layer.LoadAllServices();
        }

        /// <summary>
        /// Loads a specific pet service by id
        /// </summary>
        /// <param name="id">the id of the service that should be loaded</param>
        /// <returns></returns>
        [HttpGet]
        [Route("service/{id}")]
        public PetServiceModel Get(int id)
        {
            return _layer.LoadService(id);
        }

        /// <summary>
        /// let's you create a new service
        /// </summary>
        /// <param name="token">the login toke of the sitter</param>
        /// <param name="serviceModel">service that should be created</param>
        [HttpPost]
        [Route("service/{token}")]
        [AcceptVerbs("OPTIONS")]
        public PetServiceModel CreateService(string token, [FromBody]PetServiceModel serviceModel)
        {
            return _layer.CreateService(token, serviceModel);
        }

        /// <summary>
        /// let's you create a new service
        /// </summary>
        /// <param name="serviceModel">service that should be created</param>
        [HttpPut]
        [Route("service/{id}")]
        [AcceptVerbs("OPTIONS")]
        public PetServiceModel UpdateService(int id, [FromBody]PetServiceModel serviceModel)
        {
            return _layer.UpdateService(id, serviceModel);
        }

        /// <summary>
        /// let's you delete a service
        /// </summary>
        /// <param name="id">the id of the service that should be removed</param>
        [HttpDelete]
        [Route("service/{id}")]
        [AcceptVerbs("OPTIONS")]
        public void Delete(int id)
        {
            _layer.DeleteService(id);
        }
    }
}
