﻿using System.ComponentModel.DataAnnotations;

namespace PetBuddyServices.Models
{
    public class PetModel
    {
        public PetModel()
        {
        }
        
        public int Petid { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Petname { get; set; }

        public string Description { get; set; }

        public decimal? Weight { get; set; }
        
        [MaxLength(1)]
        public string Sex { get; set; }
        
        public string Health { get; set; }

        [Required]
        public string Pettype { get; set; }
    }
}