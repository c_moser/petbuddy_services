﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;

namespace PetBuddyServices.Models
{
    public class LogContext
    {
        public string ActionName { get; set; }

        public string ControllerName { get; set; }

        [JsonIgnore]
        public Exception Exception { get; set; }

        public string ExceptionType
        {
            get
            {
                if (Exception != null)
                {
                    return Exception.GetType().Name;
                }
                return String.Empty;
            }
        }

        public string ExceptionMessage
        {
            get
            {
                return Exception != null ? Exception.Message : string.Empty;
            }
        }

        public string ExceptionStackTrace
        {
            get
            {
                return Exception != null && !String.IsNullOrEmpty(Exception.ToString()) ? new String(Exception.ToString().Take(2000).ToArray()) : string.Empty;
            }
        }

        public string ServerName { get; set; }

        public string UserAgent { get; set; }
    }
}