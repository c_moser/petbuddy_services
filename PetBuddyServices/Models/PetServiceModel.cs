﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PetBuddyServices.Models
{
    public class PetServiceModel
    {
        public PetServiceModel()
        {
        }
    
        public int Petserviceid { get; private set; }

        [Required]
        [MaxLength(10)]
        public string Postcode { get; set; }

        [Required]
        public string Pettype { get; set; }

        public string Description { get; set; }

        [Required]
        public DateTime Start { get; set; }

        [Required]
        public DateTime End { get; set; }

        [Required]
        [MaxLength(20)]
        public string Servicetype { get; set; }

        [MaxLength(20)]
        public string Requesttype { get; set; }
    }
}