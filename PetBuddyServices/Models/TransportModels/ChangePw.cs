﻿namespace PetBuddyServices.Models.TransportModels
{
    public class ChangePw
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}