﻿namespace PetBuddyServices.Models.TransportModels
{
    public class Login
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}