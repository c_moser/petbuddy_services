﻿using System;

namespace PetBuddyServices.Models.TransportModels
{
    public class TokenData
    {
        public string Token { get; set; }

        public DateTime ValidUntil { get; set; }
    }
}