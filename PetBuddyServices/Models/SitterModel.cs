﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetBuddyServices.Models
{
    public class SitterModel
    {
        public SitterModel()
        {
            PetService = new List<PetServiceModel>();
            Pet = new List<PetModel>();
        }
    
        public int Sitterid { get; set; }

        [Required]
        [MaxLength(500)]
        [EmailAddress]
        public string Email { get; set; }
        
        [MaxLength(50)]
        [Required]
        public string Password { get; set; }

        [Required]
        [MaxLength(100)]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(100)]
        public string Lastname { get; set; }

        public string Description { get; set; }

        [MaxLength(150)]
        public string Street { get; set; }

        [MaxLength(10)]
        public string Postcode { get; set; }
        
        public DateTime? Dob { get; set; }

        [MaxLength(1)]
        public string Sex { get; set; }
    
        public List<PetServiceModel> PetService { get; set; }
        
        public List<PetModel> Pet { get; set; }
    }
}