﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PetBuddyServices.Persistance
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class petbuddyEntities : DbContext
    {
        public petbuddyEntities()
            : base("name=petbuddyEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Pet> Pets { get; set; }
        public DbSet<PetService> PetServices { get; set; }
        public DbSet<Sitter> Sitters { get; set; }
        public DbSet<sitter_auth> sitter_auth { get; set; }
        public DbSet<sitter_token> sitter_token { get; set; }
    }
}
