//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PetBuddyServices.Persistance
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sitter
    {
        public Sitter()
        {
            this.Pet = new HashSet<Pet>();
            this.PetService = new HashSet<PetService>();
            this.sitter_token = new HashSet<sitter_token>();
        }
    
        public int sitterid { get; set; }
        public string street { get; set; }
        public string postcode { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public string sex { get; set; }
        public string email { get; set; }
        public string description { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    
        public virtual ICollection<Pet> Pet { get; set; }
        public virtual sitter_auth sitter_auth { get; set; }
        public virtual ICollection<PetService> PetService { get; set; }
        public virtual ICollection<sitter_token> sitter_token { get; set; }
    }
}
