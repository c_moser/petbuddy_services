﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace PetBuddyServices.Persistance
{
    public class DbPersist
    {
        private readonly petbuddyEntities _db;

        public DbPersist()
        {
            _db = new petbuddyEntities();
        }

        #region Sitter

        public sitter_token Login(string email, string password)
        {
            var exists = (from sitter in _db.Sitters
                          join auth in _db.sitter_auth on sitter.sitterid equals auth.sitterid
                          where sitter.email == email && auth.password == password
                          select sitter);

            if(!exists.Any())
            {
                throw new Exception("false authentication");    
            }

            string token = GenerateToken(20);
            var tmpToken = new sitter_token
                               {
                                   token = token,
                                   enddate = DateTime.UtcNow.AddHours(12),
                                   //Sitter = exists.Single(),
                                   sitterid = exists.Single().sitterid
                               };
            _db.sitter_token.Add(tmpToken);
            _db.SaveChanges();

            return tmpToken;
        }

        public Sitter LoadSitter(string token)
        {
            Sitter ret = (from sitter in _db.Sitters
                          join tokenTab in _db.sitter_token on sitter.sitterid equals tokenTab.sitterid
                          where tokenTab.token == token && tokenTab.enddate > DateTime.UtcNow
                          select sitter).SingleOrDefault();

            if(ret == null)
                throw new Exception("the token is not valid anymore");

            return ret;
        }

        public Sitter CreateSitter(Sitter sitter, sitter_auth auth)
        {
            if(_db.Sitters.Any(s => s.email == sitter.email))
                throw new Exception("Email already used");

            if(string.IsNullOrEmpty(auth.password))
                throw new Exception("password is empty");

            Sitter newSitter = _db.Sitters.Add(sitter);
            auth.sitterid = newSitter.sitterid;
            _db.sitter_auth.Add(auth);
            _db.SaveChanges();

            return newSitter;
        }

        public Sitter UpdateSitter(string token, Sitter updateSitter)
        {
            var sitterEntity = LoadSitter(token);

            if (sitterEntity == null)
                throw new Exception("updateSitter not found");

            if (_db.Sitters.Any(s => s.email == updateSitter.email && s.sitterid != sitterEntity.sitterid))
                throw new Exception("Email already used");

            sitterEntity.dob = updateSitter.dob;
            sitterEntity.email = updateSitter.email;
            sitterEntity.firstname = updateSitter.firstname;
            sitterEntity.lastname = updateSitter.lastname;
            sitterEntity.description = updateSitter.description;
            sitterEntity.postcode = updateSitter.postcode;
            sitterEntity.sex = updateSitter.sex;
            sitterEntity.street = updateSitter.street;

            _db.SaveChanges();

            return _db.Sitters.SingleOrDefault(s => s.sitterid == sitterEntity.sitterid);
        }

        public void ChangePassword(string token, string oldPw, string newPw)
        {
            var sitterEntity = LoadSitter(token);

            if (sitterEntity != null)
            {
                sitter_auth auth =
                    _db.sitter_auth.SingleOrDefault(sa => sa.sitterid == sitterEntity.sitterid && sa.password == oldPw);

                if (auth == null)
                    throw new Exception("authentication is wrong");

                auth.password = newPw;
                _db.SaveChanges();
            }
            else
            {
                throw new Exception("ungueltiger token");
            }
        }

        #endregion

        #region Pet

        public Sitter AddPet(string token, Pet newPet)
        {
            var sitterEntity =
                (from sitter in _db.Sitters
                join tokenTab in _db.sitter_token on sitter.sitterid equals tokenTab.sitterid
                where tokenTab.token == token && tokenTab.enddate > DateTime.UtcNow
                select sitter).SingleOrDefault();

            newPet.sitterid = sitterEntity.sitterid;

            _db.Pets.Add(newPet);
            _db.SaveChanges();

            return LoadSitter(token);
        }

        public Sitter UpdatePet(string token, int petid, Pet updatePet)
        {
            var petEntity =
                (from sitter in _db.Sitters
                 join tokenTab in _db.sitter_token on sitter.sitterid equals tokenTab.sitterid
                 join pets in _db.Pets on sitter.sitterid equals pets.sitterid
                 where tokenTab.token == token 
                    && tokenTab.enddate > DateTime.UtcNow
                    && pets.petid == petid
                 select pets).SingleOrDefault();

            if(petEntity != null)
            {
                petEntity.description = updatePet.description;
                petEntity.health = updatePet.health;
                petEntity.petname = updatePet.petname;
                petEntity.pettype = updatePet.pettype;
                petEntity.sex = updatePet.sex;
                petEntity.weight = updatePet.weight;

                _db.SaveChanges();
            }
            else
            {
                throw new Exception("pet not found");
            }

            return LoadSitter(token);
        }

        public void DeletePet(string token, int petid)
        {
            var petEntity =
                (from sitter in _db.Sitters
                 join tokenTab in _db.sitter_token on sitter.sitterid equals tokenTab.sitterid
                 join pets in _db.Pets on sitter.sitterid equals pets.sitterid
                 where tokenTab.token == token
                    && tokenTab.enddate > DateTime.UtcNow
                    && pets.petid == petid
                 select pets).SingleOrDefault();

            if (petEntity != null)
            {
                _db.Pets.Remove(petEntity);
                _db.SaveChanges();
            }
        }

        #endregion

        #region PetService

        public List<PetService> LoadAllServices()
        {
            return _db.PetServices.ToList();
        }

        public PetService LoadService(int serviceId)
        {
            PetService service = _db.PetServices.SingleOrDefault(ps => ps.petserviceid == serviceId);

            if(service == null)
                throw new Exception("not found");

            return service;
        }

        public void DeleteService(int serviceId)
        {
            var service = _db.PetServices.SingleOrDefault(ps => ps.petserviceid == serviceId);

            if(service != null)
            {
                _db.PetServices.Remove(service);
                _db.SaveChanges();
            }
        }

        public PetService CreateService(string token, PetService newService)
        {
            Sitter sitter = LoadSitter(token);

            newService.sitterid = sitter.sitterid;

            var service = _db.PetServices.Add(newService);
            _db.SaveChanges();

            return service;
        }

        public PetService UpdateService(int id , PetService newService)
        {
            var service = LoadService(id);
            
            if(service != null)
            {
                service.description = newService.description;
                service.end = newService.end;
                service.pettype = newService.pettype;
                service.postcode = newService.postcode;
                service.requesttype = newService.requesttype;
                service.servicetype = newService.servicetype;
                service.start = newService.start;

                _db.SaveChanges();
            }
            else
                throw new Exception("service not found");

            return LoadService(id);
        }

        #endregion

        #region Helper

        private string GenerateToken(int length)
        {

            const string chars = "qwertzuioplkjhgfdsayxcvbnm0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                                  .Select(s => s[random.Next(s.Length)]).ToArray());

        }

        #endregion
    }
}