﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using PetBuddyServices.Models;
using log4net;
using log4net.Repository.Hierarchy;

namespace PetBuddyServices.Filter
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILog _logger = LogManager.GetLogger("ErrorLogger");

        public override void OnException(HttpActionExecutedContext context)
        {
            var logContext = new LogContext {Exception = context.Exception};

            HttpActionContext actionContext = context.ActionContext;
            if (actionContext != null) {
                if (actionContext.ActionDescriptor != null)
                {
                    logContext.ActionName = actionContext.ActionDescriptor.ActionName;
                    logContext.ServerName = Environment.MachineName;
                }

                if (actionContext.ControllerContext != null)
                {
                    if (actionContext.ControllerContext.ControllerDescriptor != null)
                    {
                        logContext.ControllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName;
                    }
                }
            }

            try
            {
                logContext.UserAgent = context.Request.Headers.UserAgent.ToString();
            }
            catch
            {
                logContext.UserAgent = "Not Found";
            }

            _logger.Error(JsonConvert.SerializeObject(logContext));
        }
    }
}