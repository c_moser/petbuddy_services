﻿using System.Web.Http.Cors;
using PetBuddyServices.Attributes;
using System.Web.Http;

namespace PetBuddyServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            
            config.MapHttpAttributeRoutes();
            config.Filters.Add(new CheckModelForNullAttribute());
            config.Filters.Add(new ValidateModelLogicalRestrictions());
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
