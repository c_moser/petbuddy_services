﻿using System.Web.Http;
using AutoMapper;
using PetBuddyServices.Models;
using PetBuddyServices.Persistance;
using System.IO;
using System;
using log4net.Config;
using PetBuddyServices.Filter;

namespace PetBuddyServices
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            InitializeMapping();
            GlobalConfiguration.Configuration.Filters.Add(new ExceptionFilter());
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //GlobalConfiguration.Configuration.EnableSwagger(c => c.SingleApiVersion("v1", "A title for your API")).
            //    EnableSwaggerUi();
            //GlobalConfiguration.Configuration.EnsureInitialized(); 

            var configFileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + @"log4net.config");
            XmlConfigurator.ConfigureAndWatch(configFileInfo);
        }

        private void InitializeMapping()
        {
            Mapper.Configuration.AllowNullCollections = true;
            Mapper.Configuration.AllowNullDestinationValues = true;

            Mapper.CreateMap<SitterModel, Sitter>();
            Mapper.CreateMap<Sitter, SitterModel>();
            Mapper.CreateMap<SitterModel, sitter_auth>();
            Mapper.CreateMap<PetModel, Pet>();
            Mapper.CreateMap<Pet, PetModel>();
            Mapper.CreateMap<PetService, PetServiceModel>();
            Mapper.CreateMap<PetServiceModel, PetService>();
        }
    }
}